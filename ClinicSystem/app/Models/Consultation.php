<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Consultation extends Model
{
	protected $table = 'consultation';

    protected $fillable = ['patient_id','employee_id','date','details','status','prescription'];

	public function patient()
    {
    	return $this->belongsTo(PatientAccount::class);
    }

}
