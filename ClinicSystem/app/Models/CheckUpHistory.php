<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CheckUpHistory extends Model
{
    protected $table = 'checkup_history';

    protected $fillable = ['date','details','diagnosis','prescription', 'patient_id', 'employee_id'];

	public function patient()
    {
    	return $this->belongsTo(PatientAccount::class);
    }

    public function employee()
    {
    	return $this->belongsTo(EmployeeAccount::class);
    }
}
