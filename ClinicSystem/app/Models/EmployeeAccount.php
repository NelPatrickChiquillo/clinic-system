<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class EmployeeAccount extends Authenticatable implements JWTSubject
{
	protected $table = 'employee_account';

    protected $fillable = [
        'username',
        'password',
        'name',
        'gender', 
        'designation', 
        'contact', 
        'last_login'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

	public function checkup_history()
    {
    	return $this->hasMany(CheckUpHistory::class);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier(){
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims(){
        
        return[];
    }

}
