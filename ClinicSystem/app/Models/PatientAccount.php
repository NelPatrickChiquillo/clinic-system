<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class PatientAccount extends Authenticatable implements JWTSubject
{
    protected $table = 'patient_account';

    protected $fillable = [
        'username',
        'password',
        'first_name',
        'middle_name', 
        'last_name', 
        'address', 
        'age',
        'birthday',
        'gender',
        'contact',
        'height',
        'weight',
        'blood_type',
        'condition',
        'last_checkup',
    ];

    public function consultation()
    {
        return $this->hasMany(Consultation::class);
    }

    public function checkup_history()
    {
        return $this->hasMany(CheckUpHistory::class);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier(){
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims(){
        
        return[];
    }

}
