<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

class EmployeeAccountFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => array('required', 'string', 'regex:/(^([a-zA-Z ]+$))/'),
            'username' => array('required', 'string', 'min: 5', 'max: 20'),
            'password' => array('required', 'string', 'min: 8'),
            'gender' => 'required',
            'designation' => 'required',
            'contact' => array('required', 'string', 'min:11', 'max:11'),
            'last_login' => 'bail'

        ];

    }

    public function messages()
    {
        return [ 
            'name.required' => 'ERROR: Your account must have a name',
            'name.regex' => 'WARNING: Your account name should not contain numbers or non-alphabetic symbols',
            'username.required' => 'ERROR: Your account must have a unique username',
            'username.min' => 'WARNING: Your username must be minimum of 5 characters',
            'username.max' => 'WARNING: Your username must be maximum of 20 characters',
            'password.required' => 'ERROR: Your account must have a password',
            'password.min' => 'WARNING: Your password should be a minimum of 8 characters',
            'gender.required' => 'ERROR: You cannot leave the gender field blank',
            'designation.required' => 'ERROR: You cannot leave the designation field blank',
            'contact.required' => 'ERROR: Your account should have a contact number',
            'contact.min' => 'WARNING: Your contact number should be 11-digits',
            'contact.max' => 'WARNING: Your contact number should not exceed 11-digits',
        ];

    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
        
    }
}
