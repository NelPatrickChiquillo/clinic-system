<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

class PatientAccountFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => array('required', 'string', 'min:5', 'max:20'),
            'password' => array('required', 'string', 'min: 8'),
            'first_name' => array('required', 'string', 'regex:/(^([a-zA-Z ]+$))/'),
            'middle_name' => array('string', 'regex:/(^([a-zA-Z ]+$))/'),
            'last_name' => array('required', 'string', 'regex:/(^([a-zA-Z ]+$))/'),
            'address' => array('required', 'string'),
            'age' => array('required'),
            'birthday' => array('required', 'string'),
            'gender' => 'required',
            'contact' => array('required', 'string', 'min:11', 'max:11'),
            'height' => array('required', 'numeric'),
            'weight' => array('required', 'numeric'),
            'blood_type' => 'required',
            'condition' => array('required', 'string'),
            'last_checkup' => 'bail'

        ];
        
    }

    public function messages()
    {
        return [ 
            'username.required' => 'ERROR: Your account must have a unique username',
            'username.min' => 'WARNING: Your username must be minimum of 5 characters',
            'username.max' => 'WARNING: Your username must be maximum of 20 characters',
            'password.required' => 'ERROR: Your account must have a password',
            'password.min' => 'WARNING: Your password should be a minimum of 8 characters',
            'first_name.required' => 'ERROR: Your account must have a first name',
            'first_name.regex' => 'WARNING: Your first name should not contain numbers or non-alphabetic symbols',
            'middle_name.regex' => 'WARNING: Your middle name should not contain numbers or non-alphabetic symbols',
            'last_name.required' => 'ERROR: Your account must have a last name',
            'last_name.regex' => 'WARNING: Your last name should not contain numbers or non-alphabetic symbols',
            'address.required' => 'ERROR: You cannot leave the address field blank',
            'gender.required' => 'ERROR: You cannot leave the gender field blank',
            'designation.required' => 'ERROR: You cannot leave the designation field blank',
            'contact.required' => 'ERROR: Your account should have a contact number',
            'contact.min' => 'WARNING: Your contact number should be 11-digits',
            'contact.max' => 'WARNING: Your contact number should not exceed 11-digits',
            'height.numeric' => 'WARNING: Height should be a number',
            'height.numeric' => 'WARNING: Weight should be a number'
        ];

    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));

    }
}
