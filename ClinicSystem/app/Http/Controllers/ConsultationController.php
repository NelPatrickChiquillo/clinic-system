<?php

namespace App\Http\Controllers;

use App\Models\Consultation;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

class ConsultationController extends Controller
{
    
	public function index()
    {
        $getConsultation = Consultation::with('patient')->get();

        return response()->json([
            'data' => $getConsultation

        ], 200);
    }

    public function getPendingTickets()
    {
        $id = 1;

        $pendingTickets = DB::table('consultation')
            ->where('patient_id', '=', $id)
            ->where('status', '=', 'Pending')
            ->get();

        return response()->json([
            'data' => $pendingTickets,

        ], 200);
    }

      public function store(Request $request)
    {
        $newticket = $request->all();
        $date = date('Y-m-d H:i:s');
        $newticket['date'] = $date;

        Consultation::create($newticket);

        return response()->json([
            'message' => 'Consultation ticket created successfully!',
            'data' => $newticket

        ], 201);
        
    }


    public function update(Request $request, $consultation)
    {
        try {

            $updateconsultation = Consultation::findOrFail($consultation);
            $updateconsultation->update($request->all());

            return response()->json([
            'message' => 'Consultation ticket closed successfully!',
            'data' => $updateconsultation

            ], 201);

        }catch(ModelNotFoundException $exception) {

            return response()->json([
            'message' => 'Consultation ticket not found'

            ], 404);
        }
    }

    public function destroy($consultation)
    {
        try {

            $deleteConsultation = Consultation::where('id', $consultation)->firstOrFail();
            $deleteConsultation->delete();

            return response()->json([
                'message' => 'Pending consultation ticket deleted successfully!'

            ], 201);

        }catch(ModelNotFoundException $exception) {
            return response()->json([
                'message' => 'Consultation ticket not found'
                
            ], 404);
        }
    }

}
