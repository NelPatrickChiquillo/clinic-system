<?php

namespace App\Http\Controllers;

use App\Models\PatientAccount;
use App\Models\EmployeeAccount;
use App\Models\Consultation;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

class AdminDashboardController extends Controller
{
        
    public function getDashboardData()
    {
        $patient_count = PatientAccount::count();
        $employee_count = EmployeeAccount::count();
        $consultation_count = Consultation::where('status', '=', 'Queued')->count();

        $recent_logins = EmployeeAccount::select('name', 'last_login')->where('designation', '=', 'System Administrator')->latest()->take(5)->get();

        return response()->json([
        	'patient_count' => $patient_count, 
        	'employee_count' => $employee_count, 
        	'consultation_count' => $consultation_count,
        ]);

    }

    public function getRecentLogins(){

    	$recent_logins = EmployeeAccount::select('name', 'last_login')->where('designation', '=', 'System Administrator')->latest()->take(5)->get();

    	return response()->json([
        	'recent_logins' => $recent_logins
        ]);

    }

    public function getChartData(){

    	$queued_consultations = Consultation::select(DB::raw('count(id) as `count`'), DB::raw('MONTHNAME(date) month, YEAR(date) year'))
    		->where('status', '=', 'Pending')
    		->groupBy('year', 'month')
    		->get();
    	$opened_consultations = Consultation::select(DB::raw('count(id) as `count`'), DB::raw('MONTHNAME(date) month, YEAR(date) year'))
    		->where('status', '=', 'Closed')
    		->groupBy('year', 'month')
    		->get();

    	$consultations = Consultation::select(DB::raw('MONTHNAME(date) month, YEAR(date) year'))
    		->groupBy('year', 'month')
    		->get();


    	return response()->json([
        	'queued' => $queued_consultations,
        	'answered' => $opened_consultations,
        	'consultations' => $consultations
        ]);
    	
   	}
}
