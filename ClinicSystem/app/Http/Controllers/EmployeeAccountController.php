<?php

namespace App\Http\Controllers;

use App\Models\EmployeeAccount;
use App\Http\Requests\EmployeeAccountFormRequest;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Carbon\Carbon;


class EmployeeAccountController extends Controller
{

    public function index(){

        return response()->json(EmployeeAccount::all());

    }

    public function store(EmployeeAccountFormRequest $request)
    {
        $newaccount = $request->validated();
        $newaccount['password'] = Hash::make($request['password']);
        EmployeeAccount::create($newaccount);

        return response()->json([
            'message' => 'Employee account created successfully!',
            'data' => $newaccount

        ], 201);
        
    }

    public function update(Request $request, $employee)
    {
        try {

            $editaccount = EmployeeAccount::findOrFail($employee);
            $request['password'] = Hash::make($request['password']);
            $editaccount->update($request->all());

            return response()->json([
            'message' => 'Employee account updated successfully!',
            'data' => $editaccount

            ], 201);

        }catch(ModelNotFoundException $exception) {

            return response()->json([
            'message' => 'Employee account not found'

            ], 404);
        }
    }

    public function destroy($employee)
    {
        try {
            $deleteAccount = EmployeeAccount::where('id', $employee)->firstOrFail();
            $deleteAccount->delete();

            return response()->json([
                'message' => 'Employee account deleted successfully!'

            ], 201);

        }catch(ModelNotFoundException $exception) {
            return response()->json([
                'message' => 'Employee account not found'
                
            ], 404);
        }
    }

}

