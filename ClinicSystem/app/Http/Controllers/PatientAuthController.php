<?php

namespace App\Http\Controllers;

use App\Models\PatientAccount;
use App\Http\Requests\PatientAccountFormRequest;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Validator;

class PatientAuthController extends Controller
{

	public function login(Request $request)
	{
		$credentials = Validator::make($request->all(), [
			'username' => 'required|string',
			'password' => 'required'

		]);
 
		if($credentials->fails())
		{
			return response()->json($credentials->messages(), 422);
		}

        if(!$token = auth('patients')->attempt($credentials->validated()))
        {
            return response()->json(['error' => 'Invalid user credentials'], 401);
        }

        return $this->respondWithToken($token);
	}

	public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth('patients')->invalidate(Request()->token);
        auth()->logout();
        return response()->json(['message' => 'Account logged out successfully!']);
    }

    /**
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 1000000,
            'user' => auth('patients')->user()
        ]);
    }

}

