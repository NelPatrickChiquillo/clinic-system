<?php

namespace App\Http\Controllers;

use App\Models\PatientAccount;

use App\Http\Requests\PatientAccountFormRequest;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facade\Auth;
use Validator;

class PatientAccountController extends Controller
{
	
	public function index()
    {

        return response()->json(PatientAccount::all());

    }

    public function store(PatientAccountFormRequest $request)
    {
        $newaccount = $request->validated();
        $newaccount['password'] = Hash::make($request['password']);
        PatientAccount::create($newaccount);

        return response()->json([
            'message' => 'Patient account created successfully!',
            'data' => $newaccount

        ], 201);
        
    }

    public function update(Request $request, $patient)
    {
        try {

            $editPatient = PatientAccount::findOrFail($patient);
            $editPatient->update($request->all());

            return response()->json([
            'message' => 'Patient details updated successfully!',
            'data' => $editPatient

            ], 201);

        }catch(ModelNotFoundException $exception) {

            return response()->json([
            'message' => 'Patient details not found'

            ], 404);
        }
    }

    public function destroy($patient)
    {
        try {
            $deletePatient = PatientAccount::where('id', $patient)->firstOrFail();
            $deletePatient->delete();

            return response()->json([
                'message' => 'Patient details deleted successfully!',
                'data' => $deletePatient

            ], 201);

        }catch(ModelNotFoundException $exception) {
            return response()->json([
                'message' => 'Patient details not found'
                
            ], 404);
        }
    }

}
