<?php

namespace App\Http\Controllers;

use App\Models\CheckUpHistory;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CheckUpHistoryController extends Controller
{

    public function getClosedTickets()
    {
        $id = 1;
        $closedTickets = CheckUpHistory::with('employee')->where('patient_id', $id)->get();

        return response()->json([
            'data' => $closedTickets

        ], 200);
    }
   
    public function store(Request $request)
    {
        $newcheckup = $request->all();
        $date = date('Y-m-d H:i:s');
        $newcheckup['date'] = $date;

        CheckUpHistory::create($newcheckup);

        return response()->json([
            'message' => 'Checkup history updated successfully!',
            'data' => $newcheckup

        ], 201);
        
    }

    public function destroy($consultation)
    {
        try {

            $deleteConsultation = CheckUpHistory::where('id', $consultation)->firstOrFail();
            $deleteConsultation->delete();

            return response()->json([
                'message' => 'Closed consultation ticket deleted successfully!'

            ], 201);

        }catch(ModelNotFoundException $exception) {
            return response()->json([
                'message' => 'Consultation ticket not found'
                
            ], 404);
        }
    }

}
