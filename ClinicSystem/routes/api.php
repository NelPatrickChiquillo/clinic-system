<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\PatientAccountController;
use App\Http\Controllers\PatientAuthController;
use App\Http\Controllers\EmployeeAccountController;
use App\Http\Controllers\AdminDashboardController;
use App\Http\Controllers\ConsultationController;
use App\Http\Controllers\CheckUpHistoryController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::group([
	'prefix' => 'auth'
], function(){
	Route::post('employee/register', 'EmployeeAccountController@register');
	Route::post('employee/login', 'EmployeeAccountController@login');
	Route::post('patient/register', 'PatientAccountController@register');
	Route::post('patient/login', 'PatientAccountController@login');
});*/


/*Route::group(['prefix' => 'employees', 'middleware' => ['auth.guard:employees']], function(){

	Route::post('register', 'EmployeeAccountController@register');
	Route::post('login', 'EmployeeAccountController@login');
});

Route::group(['prefix' => 'patients', 'middleware' => ['auth.guard:patients']], function(){

	Route::post('register', 'PatientAccountController@register');
	Route::post('login', 'PatientAccountController@login');
}); */

Route::group(['middleware' => 'api', 'prefix' => 'auth'], function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('me', [AuthController::class, 'me']);
});

Route::group(['middleware' => 'api', 'prefix' => 'patientauth'], function () {
    Route::post('login', [PatientAuthController::class, 'login']);
    Route::post('logout', [PatientAuthController::class, 'logout']);
    Route::post('refresh', [PatientAuthController::class, 'refresh']);
    Route::post('me', [PatientAuthController::class, 'me']);

});

Route::group(['middleware' => 'api'], function () {
  	Route::get('dashboard_data', [AdminDashboardController::class, 'getDashboardData']);
    Route::get('dashboard_data/recent_logins', [AdminDashboardController::class, 'getRecentLogins']);
    Route::get('dashboard_data/chart_data', [AdminDashboardController::class, 'getChartData']);

    Route::resource('employees', 'EmployeeAccountController', [
	'only' => ['index','store','update','destroy']
	]);

	Route::resource('patients', 'PatientAccountController', [
	'only' => ['index','store','update','destroy']
	]);

	Route::resource('consultations', 'ConsultationController', [
	'only' => ['index','store','update','destroy']
	]);

	Route::get('consultations/pending', [ConsultationController::class, 'getPendingTickets']);
	Route::get('consultations/closed', [CheckUpHistoryController::class, 'getClosedTickets']);

	Route::resource('checkup', 'CheckUpHistoryController', [
	'only' => ['index','store','update','destroy']
	]);

});

Route::get('printpdf', array('as' => 'printpdf',
	'uses' => 'GeneratePDFController@printpdf'
));


