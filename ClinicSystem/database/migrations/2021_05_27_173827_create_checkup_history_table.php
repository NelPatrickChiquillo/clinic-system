<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheckupHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkup_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('patient_id')->unsigned();
            $table->foreign('patient_id')
                  ->references('id')->on('patient_account')
                  ->onDelete('cascade');
            $table->integer('employee_id')->unsigned();
            $table->foreign('employee_id')
                  ->references('id')->on('employee_account')
                  ->onDelete('cascade');
            $table->string('date');
            $table->longText('details');
            $table->string('diagnosis');
            $table->string('prescription');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checkup_history');
    }
}
