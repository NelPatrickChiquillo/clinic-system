<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Models\PatientAccount;

use Carbon\Carbon;

class PatientAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('patient_account')->insert(array(
	       		array(
	       			'username' => 'patient',
	       		 	'password' => Hash::make('patient123'),
	       		 	'first_name' => 'Patient',
	       		 	'middle_name' => 'Test',
	       		 	'last_name' => 'Account',
	       		 	'address' => 'Tacloban City',
	       		 	'age' => '21',
	       		 	'birthday' => '01/01/2000',
	       		 	'gender' => 'Male',
	       		 	'contact' => '09501234567',
	       		 	'height' => '170',
	       		 	'weight' => '120',
	       		 	'blood_type' => 'A+',
	       		 	'condition' => 'Physically Fit',
	       		 	'last_checkup' => '',
	       		 	'created_at' => Carbon::now(),
	       		 	'updated_at' => Carbon::now()
	       		)
	       	)
    	);
    }
}
