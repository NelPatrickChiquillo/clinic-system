<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Models\EmployeeAccount;

use Carbon\Carbon;

class EmployeeAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employee_account')->insert(array(
	       		array(
	       			'username' => 'admin',
	       		 	'password' => Hash::make('admin123'),
	       		 	'name' => 'Admin Test',
	       		 	'gender' => 'Male',
	       		 	'designation' => 'System Administrator',
	       		 	'contact' => '09501234567',
	       		 	'last_login' => "N/A",
	       		 	'created_at' => Carbon::now(),
	       		 	'updated_at' => Carbon::now()
	       		),
	       		array(
	       			'username' => 'doctor',
	       		 	'password' => Hash::make('doctor123'),
	       		 	'name' => 'Doctor Test',
	       		 	'gender' => 'Female',
	       		 	'designation' => 'Health Worker',
	       		 	'contact' => '09501234567',
	       		 	'last_login' => "N/A",
	       		 	'created_at' => Carbon::now(),
	       		 	'updated_at' => Carbon::now()
	       		)
       		)
    	);
    }
}
