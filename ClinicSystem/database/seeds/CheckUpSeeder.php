<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Models\CheckUpHistory;

use Carbon\Carbon;

class CheckUpSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('checkup_history')->insert(array(
	       		array(
	       			'patient_id' => 1,
	       		 	'employee_id' => 2,
	       		 	'date' => Carbon::now(),
	       		 	'details' => 'Lorem ipsum dolor...',
	       		 	'diagnosis' => 'Common Cold',
	       		 	'prescription' => 'Neozep'
	       		)
	       	)
    	);
    }
}
