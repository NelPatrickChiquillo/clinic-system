<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Models\Consultation;

use Carbon\Carbon;

class ConsultationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('consultation')->insert(array(
           	    array(
           	    	'patient_id' => 1,
           	     	'date' => Carbon::now(),
           	     	'details' => 'Lorem ipsum dolor...',
           	     	'status' => 'Queued'
           	    ),
                array(
                    'patient_id' => 1,
                    'date' => Carbon::now(),
                    'details' => 'Lorem ipsum dolor...',
                    'status' => 'Opened'
                )
            )
        );
    }
}
