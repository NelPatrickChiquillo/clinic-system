require('./bootstrap');
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex'
import BootstrapVue from 'bootstrap-vue';

import MainApp from  './components/MainApp';
import Toast from "vue-toastification";

import {routes} from './routes';
import store from './store/store';

import 'vue-toastification/dist/index.css'

Vue.use(BootstrapVue);
Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(Toast);

const router = new VueRouter({
	routes,
	mode: 'history'

});

router.beforeEach((to, from, next) => {
    if (to.matched.some((record) => record.meta.requiresLogin) && !localStorage.getItem('auth')){
   		next({name: 'home'})
    }
    else if (to.matched.some((record) => record.meta.hasUser) && localStorage.getItem('auth')) {
        if(localStorage.getItem('designation') == "System Administrator"){
            next({name: 'dashboard'})
        }else if(localStorage.getItem('designation') == "Health Worker"){
            next({name: 'records'})
        }
      
	}
    else if (to.matched.some((record) => record.meta.isAdmin) && localStorage.getItem('designation') != 'System Administrator') {
       next({name: 'PageNotAuthorized'})
      
    }
    else if (to.matched.some((record) => record.meta.isHealthWorker) && localStorage.getItem('designation') != 'Health Worker') {
       next({name: 'PageNotAuthorized'})
      
    }
	else {
		next();
	}
});

const app = new Vue({
    el: '#app',
    router,
    store,
    components: {
 		MainApp
 	}
});
