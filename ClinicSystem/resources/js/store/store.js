import Vue from 'vue';
import Vuex from 'vuex';

import auth from './modules/auth/index';
import patientauth from './modules/patientauth/index';
import employees from './modules/employees/index';
import patients from './modules/patients/index';
import dashboard from './modules/dashboard/index';
import consultations from './modules/consultations/index';
import checkup from './modules/checkup/index';
//import booksModule from './modules/books'
//import categoriesModule from './modules/categories'

Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		auth,
		employees,
		patients,
		dashboard,
		consultations,
		checkup,
		patientauth
	
	}

})
