import API from '../../base/config';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default {
    namespaced: true,
    state: {
        employees: []
    },

    getters: {
        getEmployeeData: function(state) {
            return state.employees;
        }
    },

    mutations: {
        SET_EMPLOYEES(state, data){
            state.employees = data
        },
        SAVE_EMPLOYEE(state, data){
            state.employees.unshift(data)
        },
        UPDATE_EMPLOYEE(state, {index, data}){
            Vue.set(state.employees, index, data);
        },
        DELETE_EMPLOYEE(state, id){
            state.employees = state.employees.filter(employee => {
                return employee.id !== id
            })
        }
    },



    actions: {

        async getEmployees({commit}){
            const res = await API.get('/employees')
            .then(response => {
                commit ('SET_EMPLOYEES', response.data);
                return response;
            }).catch(error => {
                return error.response;
            })

            return res;
        },

        async addEmployee({commit}, data){
            const res = await API.post('/employees', data)
            .then(response => {
                commit('SAVE_EMPLOYEE', response.data)
                return response;
            })
            .catch(error => {
                return error.response;
            });
      
            return res;
        },

        async updateEmployee({commit}, {data, index}){
            const res = await API.put('/employees/' + `${data.id}`, data)
            .then(response => {
                commit('UPDATE_EMPLOYEE', {index, data})
                return response;
            })
            .catch(error => {
                return error.response
            });
     
            return res
        },

        async deleteEmployee({commit}, id){
            const res = await API.delete('/employees/' + `${id}`)
            .then(response => {
                commit('DELETE_EMPLOYEE', id)
                return response;
            })
            .catch(error => {
                return error.response;
            });
      
            return res;
        }

    }
}