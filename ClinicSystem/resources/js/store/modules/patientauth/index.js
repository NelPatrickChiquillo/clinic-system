import API from '../../base/config';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default {
    namespaced: true,
    state: {
        id: localStorage.getItem('id') || '',
        firstname: localStorage.getItem('firstname') || '',
        lastname: localStorage.getItem('lastname') || '',
        token: localStorage.getItem('auth') || ''
    },

    getters: {
    
    },

    mutations: {
        SET_AUTH_ACC(state, data) {
            localStorage.setItem('id', data.id);
            localStorage.setItem('firstname', data.first_name);
            localStorage.setItem('lastname', data.last_name);
        },
        SET_AUTH_TOKEN(state, token) {
            localStorage.setItem('auth', token)
            state.token = token

            const bearer_token = localStorage.getItem('auth') || ''
            API.defaults.headers.common['Authorization'] = `Bearer ${bearer_token}`
        },
        UNSET_USER(state){
            localStorage.removeItem('auth');
            localStorage.removeItem('firstname');
            localStorage.removeItem('lastname');
            localStorage.removeItem('id');
            state.token = ''

            API.defaults.headers.common['Authorization'] = ''
        }
    },

    actions: {

        async loginAccount({commit}, payload){
            const res = await API.post('patientauth/login', payload)
                .then(response => {
                    commit('SET_AUTH_ACC', response.data.user)
                    commit('SET_AUTH_TOKEN', response.data.access_token)
                    return response;
                }).catch(error => {
                    return error.response;
                })

            return res;
        },

        async signUpAccount({commit}, payload){
            const res = await API.post('/patientauth/register', payload)
            .then(response => {
                return response;
            }).catch(error => {
                return error.response;
            })

            return res;
        },

        async updateAccount({commit}, payload){
            const res = await API.put('/patientauth/update', payload)
                .then(response => {
                    return response;
                }).catch(error => {
                    return error.response;
                })

            return res;
        },

        async logoutUser({commit}){
            const res = await API.post('/patientauth/logout?token=' + localStorage.getItem('auth'))
                .then(response => {
                    commit('UNSET_USER')
                    return response
                }).catch(error => {
                    return error.response
                });

            return res;
        },

        async checkUser({commit}) {
            const res = await API.post('/patientauth/me?token=' + localStorage.getItem('auth'))
            .then(response => {
                commit('SET_ACC', response.data)
                return response;
            }).catch(error => {
                return error.response;
            })

            return res;
        }


    }
}