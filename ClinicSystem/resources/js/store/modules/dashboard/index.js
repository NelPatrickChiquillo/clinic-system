import API from '../../base/config';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default {
    namespaced: true,
    state: {
        data: [],
        recent_logins: [],
        chart: []
    },

    getters: {
        
    },

    mutations: {
        SET_DASHBOARD_DATA(state, data){
            state.data = data
        },

        SET_RECENT_LOGINS(state, data){
            state.recent_logins = data
        },

        SET_CHART_DATA(state, data){
            state.chart = data
        }
    },

    actions: {
        async getDashboardData({commit}){
            const res = await API.get('dashboard_data')
            .then(res => {
                commit('SET_DASHBOARD_DATA', res.data)
                return res
            }).catch(error => {
                return error.res
            });
    
            return res;
        },

        async getRecentLogins({commit}){
            const res = await API.get('dashboard_data/recent_logins')
            .then(res => {
                commit('SET_RECENT_LOGINS', res.data)
                return res
            }).catch(error => {
                return error.res
            });
    
            return res;
        },

        async getChartData({commit}){
            const res = await API.get('dashboard_data/chart_data')
            .then(res => {
                commit('SET_CHART_DATA', res.data)
                return res
            }).catch(error => {
                return error.res
            });
    
            return res;
        }

    }
}