import API from '../../base/config';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default {
    namespaced: true,
    state: {
        patients: []
    },

    getters: {
        getPatientData: function(state) {
            return state.patients;
        }
    },

    mutations: {
        SET_PATIENTS(state, data){
            state.patients = data
        },
        SAVE_PATIENT(state, data){
            state.patients.unshift(data)
        },
        UPDATE_PATIENT(state, {index, data}){
            Vue.set(state.patients, index, data);
        },
        DELETE_PATINET(state, id){
            state.patients = state.patients.filter(employee => {
                return patient.id !== id
            })
        }
    },



    actions: {

        async getPatients({commit}){
            const res = await API.get('/patients')
            .then(response => {
                commit ('SET_PATIENTS', response.data);
                return response;
            }).catch(error => {
                return error.response;
            })

            return res;
        },

        async addPatient({commit}, data){
            const res = await API.post('/patients', data)
            .then(response => {
                commit('SAVE_PATIENT', response.data)
                return response;
            })
            .catch(error => {
                return error.response;
            });
      
            return res;
        },

        async updatePatient({commit}, {data, index}){
            const res = await API.put('/patients/' + `${data.id}`, data)
            .then(response => {
                commit('UPDATE_PATIENT', {index, data})
                return response;
            })
            .catch(error => {
                return error.response;
            });
     
            return res
        },

        async deletePatient({commit}, id){
            const res = await API.delete('/patients/' + `${id}`)
            .then(response => {
                commit('DELETE_PATIENT', id)
                return response;
            })
            .catch(error => {
                return error.response;
            });
      
            return res;
        }

    }
}