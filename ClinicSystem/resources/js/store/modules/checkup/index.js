import API from '../../base/config';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default {
    namespaced: true,
    state: {
        checkup: []
    },

    getters: {
        getCheckupData: function(state) {
            return state.checkup;
        }
    },

    mutations: {
        SET_CHECKUP(state, data){
            state.checkup = data
        },
        SAVE_CHECKUP(state, data){
            state.checkup.unshift(data)
        },
        UPDATE_CHECKUP(state, {index, data}){
            Vue.set(state.checkup, index, data);
        },
        DELETE_CHECKUP(state, id){
            state.checkup = state.checkup.filter(checkup => {
                return checkup.id !== id
            })
        }
    },



    actions: {

        async getCheckup({commit}){
            const res = await API.get('/checkup')
            .then(response => {
                commit ('SET_CHECKUP', response.data);
                return response;
            }).catch(error => {
                return error.response;
            })

            return res;
        },

        async addCheckup({commit}, data){
            const res = await API.post('/checkup', data)
            .then(response => {
                commit('SAVE_CHECKUP', response.data)
                return response;
            })
            .catch(error => {
                return error.response;
            });
      
            return res;
        },

        async updateCheckup({commit}, {data, index}){
            const res = await API.put('/checkup/' + `${data.id}`, data)
            .then(response => {
                commit('UPDATE_EMPLOYEE', {index, data})
                return response;
            })
            .catch(error => {
                return error.response
            });
     
            return res
        },

        async deleteCheckup({commit}, id){
            const res = await API.delete('/checkup/' + `${id}`)
            .then(response => {
                commit('DELETE_CHECKUP', id)
                return response;
            })
            .catch(error => {
                return error.response;
            });
      
            return res;
        }

    }
}