import API from '../../base/config';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default {
    namespaced: true,
    state: {
        consultations: [],
        pending: [],
        closed: [],
        patients: [],
        employees: []
    },

    getters: {
        getConsultationData: function(state) {
            return state.consultations;
        },
        getPendingConsultationData: function(state) {
            return state.pending;
        },
        getClosedConsultationData: function(state) {
            return state.closed;
        }
    },

    mutations: {
        SET_CONSULTATIONS(state, data){
            state.consultations = data
        },
        SET_PENDING_CONSULTATIONS(state, data){
            state.pending = data
        },
        SET_CLOSED_CONSULTATIONS(state, data){
            state.closed = data
        },
        SET_PATIENTS(state, data){
            state.patients = data
        },
        SET_EMPLOYEES(state, data){
            state.employees = data
        },
        SAVE_CONSULTATION(state, data){
            state.consultations.unshift(data)
        },
        UPDATE_CONSULTATION(state, {index, data}){
            Vue.set(state.consultations, index, data);
        },
        DELETE_CONSULTATION(state, id){
            state.consultations = state.consultations.filter(consultation => {
                return consultation.id !== id
            })
        },
        DELETE_PENDING_CONSULTATION(state, id){
            state.pending = state.pending.filter(pending => {
                return pending.id !== id
            })
        },
        DELETE_CLOSED_CONSULTATION(state, id){
            state.closed = state.closed.filter(closed => {
                return closed.id !== id
            })
        }


    },


    actions: {

        async getConsultations({commit}){
            const res = await API.get('/consultations')
            .then(response => {
                commit ('SET_CONSULTATIONS', response.data.data);
                return response;
            }).catch(error => {
                return error.response;
            })

            return res;
        },

        async getPendingConsultations({commit}){
            const res = await API.get('/consultations/pending')
            .then(response => {
                commit ('SET_PENDING_CONSULTATIONS', response.data.data);
                return response;
            }).catch(error => {
                return error.response;
            })

            return res;
        },

        async getClosedConsultations({commit}){
            const res = await API.get('/consultations/closed')
            .then(response => {
                commit ('SET_CLOSED_CONSULTATIONS', response.data.data);
                return response;
            }).catch(error => {
                return error.response;
            })

            return res;
        },

        async getPatients({commit}){
            const res = await API.get('/patients')
            .then(response => {
                commit ('SET_PATIENTS', response.data);
                return response;
            }).catch(error => {
                return error.response;
            })

            return res;
        },

        async getEmployees({commit}){
            const res = await API.get('/employees')
            .then(response => {
                commit ('SET_EMPLOYEES', response.data);
                return response;
            }).catch(error => {
                return error.response;
            })

            return res;
        },

        async addConsultation({commit}, data){
            const res = await API.post('/consultations', data)
            .then(response => {
                commit('SAVE_CONSULTATION', response.data)
                return response;
            })
            .catch(error => {
                return error.response;
            });
      
            return res;
        },

        async updateConsultation({commit}, {data, index}){
            const res = await API.put('/consultations/' + `${data.id}`, data)
            .then(response => {
                commit('UPDATE_CONSULTATION', {index, data})
                return response;
            })
            .catch(error => {
                return error.response
            });
     
            return res
        },

        async deletePendingConsultation({commit}, id){
            const res = await API.delete('/consultations/' + `${id}`)
            .then(response => {
                commit('DELETE_PENDING_CONSULTATION', id)
                return response;
            })
            .catch(error => {
                return error.response;
            });
      
            return res;
        },

        async deleteClosedConsultation({commit}, id){
            const res = await API.delete('/checkup/' + `${id}`)
            .then(response => {
                commit('DELETE_CLOSED_CONSULTATION', id)
                return response;
            })
            .catch(error => {
                return error.response;
            });
      
            return res;
        }

    }
}