import Home from  './components/Home';
import PatientLogin from  './components/PatientLogin';
import PatientRegistration from  './components/PatientRegistration';
import StaffLogin from  './components/StaffLogin';

import AdminDashboard from  './components/admin/AdminDashboard';
import EmployeesAccount from  './components/admin/EmployeesAccount';
import PatientsAccount from  './components/admin/PatientsAccount';

import PatientRecords from  './components/doctors/PatientRecords';
import PatientConsultation from  './components/doctors/PatientConsultation';

import PatientPanel from  './components/patient/PatientPanel';
import PatientProfile from  './components/patient/PatientProfile';

import PageNotFound from './components/PageNotFound';
import PageNotAuthorized from './components/PageNotAuthorized';

export const routes = [
    {
        path:'/'  ,
        name: 'home',
        component: Home,
        meta:{
            hasUser: true
        }
    },
    {
        path:'/patient_login',
        name: 'patient_login',
        component: PatientLogin,
        meta:{
            transitionName: 'slide',
            hasUser: true
        }
    },
    {
        path:'/patient_registration',
        name: 'patient_registration',
        component: PatientRegistration,
        meta:{
            transitionName: 'slide',
            hasUser: true
        }
    },
    {
        path:'/staff_login',
        name: 'staff_login',
        component: StaffLogin,
        meta:{
            transitionName: 'slide',
            hasUser: true
        }
    },
    {
        path:'/system_administrator/dashboard',
        name: 'dashboard',
        component: AdminDashboard,
        meta:{
            transitionName: 'none',
            requiresLogin: true,
            isAdmin: true
        }
    },
    {
        path:'/system_administrator/employees_account',
        component: EmployeesAccount,
        meta:{
            transitionName: 'none',
            requiresLogin: true,
            isAdmin: true
        }
    },
    {
        path:'/system_administrator/patients_account',
        component: PatientsAccount,
        meta:{
            transitionName: 'none',
            requiresLogin: true,
            isAdmin: true
        }
    },
    {
        path:'/doctors/patient_records',
        component: PatientRecords,
        name: 'records',
        meta:{
            transitionName: 'none',
            requiresLogin: true,
            isHealthWorker: true
        }
    },
    {
        path:'/doctors/patient_consultation',
        component: PatientConsultation,
        meta:{
            transitionName: 'none',
            requiresLogin: true,
            isHealthWorker: true
        }
    },
    {
        path:'/patient/patient_panel',
        component: PatientPanel,
        meta:{
            transitionName: 'none',
            requiresLogin: true,

        }
    },
    {
        path:'/patient/patient_profile',
        component: PatientProfile,
        meta:{
            transitionName: 'none',
            requiresLogin: true,
        }
    },
    {
        path:'*',
        name: 'PageNotFound',
        component: PageNotFound
    },
    {
        path:'/not_authorized',
        name: 'PageNotAuthorized',
        component: PageNotAuthorized
    },



];